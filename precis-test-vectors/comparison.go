// Copyright 2016 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/xml"
	"fmt"
	"strconv"

	. "golang.org/x/text/secure/precis"
)

var CompareTestCases = []CompareCases{
	{"Nickname", Nickname, []compareTestCase{
		{"a", "b", false},
		{"  Swan  of   Avon   ", "swan of avon", true},
		{"Foo", "foo", true},
		{"foo", "foo", true},
		{"Foo Bar", "foo bar", true},
		{"foo bar", "foo bar", true},
		{"\u03A3", "\u03C3", true},
		{"\u03A3", "\u03C2", false},
		{"\u03C3", "\u03C2", false},
		{"Richard \u2163", "richard iv", true},
		{"Å", "å", true},
		{"ﬀ", "ff", true}, // because of NFKC
		{"ß", "sS", false},

		// After applying the Nickname profile, \u00a8  becomes \u0020\u0308,
		// however because the nickname profile is not idempotent, applying it again
		// to \u0020\u0308 results in \u0308.
		{"\u00a8", "\u0020\u0308", true},
		{"\u00a8", "\u0308", true},
		{"\u0020\u0308", "\u0308", true},
	}},
}

type compareTestCase struct {
	A      string
	B      string
	Result bool
}

type CompareCases struct {
	Name  string
	P     *Profile
	Cases []compareTestCase
}

func (ec CompareCases) TokenReader() xml.TokenReader {
	var cases []xml.TokenReader
	for _, c := range ec.Cases {
		cases = append(cases, tr(
			td(fmt.Sprintf("%x", c.A)),
			td(fmt.Sprintf("%x", c.B)),
			td(strconv.FormatBool(c.Result)),
		))
	}

	return wrapSection(ec.Name,
		table(
			thead(
				tr(
					th("A"),
					th("B"),
					th("Result"),
				),
			),
			tbody(cases...),
		),
	)
}
