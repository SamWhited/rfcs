// Copyright 2016 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// The genprecistestvec command spits out files that will be included in the
// PRECIS test vector RFCs. It checks easy test against the
// golang.org/x/text/secure/precis package to make sure they are all valid
// first and may also generate files to test against other implementations.
//
// For more information see genprecistestvec -help
package main // import "blog.samwhited.com/static/rfcs/genprecistestvec"

import (
	"encoding/xml"
	"errors"
	"flag"
	"log"
	"os"

	"golang.org/x/sync/errgroup"
	"mellium.im/xmlstream"
)

func main() {
	var (
		nocheck     = flag.Bool("nocheck", false, "do not verify tests against golang.org/x/text/secure/precis")
		enforcepath = flag.String("enforcexml", "enforcetests.xml", "path for enforcement tests XML output; empty for no XML output")
		comparepath = flag.String("comparexml", "comparetests.xml", "path for comparison tests XML output; empty for no XML output")
		machinepath = flag.String("json", "machinetests.json", "path for machine readable tests in JSON format; empty for no JSON output")
	)
	flag.Parse()

	if !*nocheck {
		if err := CheckTests(); err != nil {
			log.Fatal("Errors encountered while checking tests")
		}
	}
	if *enforcepath != "" {
		if err := GenEnforceTests(*enforcepath); err != nil {
			log.Fatal(err)
		}
	}
	if *comparepath != "" {
		if err := GenCompareTests(*comparepath); err != nil {
			log.Fatal(err)
		}
	}
	if *machinepath != "" {
		if err := GenMachineTests(*machinepath); err != nil {
			log.Fatal(err)
		}
	}
}

func CheckTests() error {
	var errgrp errgroup.Group

	for _, g := range EnforceTestCases {
		for i, tc := range g.Cases {
			g, tc := g, tc // https://golang.org/doc/faq#closures_and_goroutines

			errgrp.Go(func() error {
				if e, err := g.P.String(tc.Input); (tc.Err != nil) != (err != nil) || e != tc.Output {
					log.Printf("Enforce/%s/%d: got %+q (err: %v); want %+q (err: %v)", g.Name, i, e, err, tc.Output, tc.Err)
					return errors.New("Errored unexpectedly")
				}
				return nil
			})
		}
	}

	for _, g := range CompareTestCases {
		for i, tc := range g.Cases {
			g, tc := g, tc

			errgrp.Go(func() error {
				if result := g.P.Compare(tc.A, tc.B); result != tc.Result {
					log.Printf("Compare/%s/%d: got %v; want %v", g.Name, i, result, tc.Result)
					return errors.New("Compare test failed")
				}
				return nil
			})
		}
	}
	return errgrp.Wait()
}

func wrapSection(name string, r xml.TokenReader) xml.TokenReader {
	return xmlstream.Wrap(
		xmlstream.MultiReader(
			xmlstream.Wrap(
				xmlstream.Token(xml.CharData(name)),
				xml.StartElement{Name: xml.Name{Local: "name"}},
			),
			r,
		),
		xml.StartElement{
			Name: xml.Name{Local: "section"},
			Attr: []xml.Attr{
				{Name: xml.Name{Local: "numbered"}, Value: "true"},
				{Name: xml.Name{Local: "toc"}, Value: "default"},
			},
		},
	)
}

func GenEnforceTests(path string) error {
	fd, err := os.Create(path)
	if err != nil {
		return err
	}
	defer fd.Close()

	e := xml.NewEncoder(fd)
	e.Indent("", "  ")

	var cases []xml.TokenReader
	for _, g := range EnforceTestCases {
		cases = append(cases, g.TokenReader())
	}
	r := wrapSection("Enforcement Tests", xmlstream.MultiReader(cases...))
	_, err = xmlstream.Copy(e, r)
	if err != nil {
		return err
	}
	return e.Flush()
}

func GenCompareTests(path string) error {
	fd, err := os.Create(path)
	if err != nil {
		return err
	}
	defer fd.Close()

	e := xml.NewEncoder(fd)
	e.Indent("", "  ")

	var cases []xml.TokenReader
	for _, g := range CompareTestCases {
		cases = append(cases, g.TokenReader())
	}
	r := wrapSection("Comparison Tests", xmlstream.MultiReader(cases...))
	_, err = xmlstream.Copy(e, r)
	if err != nil {
		return err
	}
	return e.Flush()
}
