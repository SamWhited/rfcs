// Copyright 2016 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"os"
)

// GenMachineTests outputs machine readable test files in JSON format.
// These are then imported by testers for other PRECIS implementations for
// comparison.
func GenMachineTests(path string) error {
	fd, err := os.Create(path)
	if err != nil {
		return err
	}
	defer fd.Close()

	e := json.NewEncoder(fd)
	e.SetIndent("", "  ")

	return e.Encode(struct {
		EnforceTests []EnforceCases `json:"enforce"`
		CompareTests []CompareCases `json:"compare"`
	}{
		EnforceTests: EnforceTestCases,
		CompareTests: CompareTestCases,
	})
}
