module rfcs.samwhited.com/precis-test-vectors

go 1.13

require (
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a
	golang.org/x/text v0.3.2
	mellium.im/xmlstream v0.15.0
)
