package main

import (
	"encoding/xml"
	"mellium.im/xmlstream"
)

func table(thead, tbody xml.TokenReader) xml.TokenReader {
	return xmlstream.Wrap(
		xmlstream.MultiReader(thead, tbody),
		xml.StartElement{
			Name: xml.Name{Local: "table"},
			Attr: []xml.Attr{
				{Name: xml.Name{Local: "align"}, Value: "center"},
			},
		},
	)
}

func thead(r xml.TokenReader) xml.TokenReader {
	return xmlstream.Wrap(
		r,
		xml.StartElement{Name: xml.Name{Local: "thead"}},
	)
}

func tbody(r ...xml.TokenReader) xml.TokenReader {
	return xmlstream.Wrap(
		xmlstream.MultiReader(r...),
		xml.StartElement{Name: xml.Name{Local: "tbody"}},
	)
}

func th(name string) xml.TokenReader {
	return xmlstream.Wrap(
		xmlstream.Token(xml.CharData(name)),
		xml.StartElement{
			Name: xml.Name{Local: "th"},
			Attr: []xml.Attr{
				{Name: xml.Name{Local: "align"}, Value: "left"},
			},
		},
	)
}

func td(name string) xml.TokenReader {
	return xmlstream.Wrap(
		xmlstream.Token(xml.CharData(name)),
		xml.StartElement{
			Name: xml.Name{Local: "th"},
			Attr: []xml.Attr{
				{Name: xml.Name{Local: "align"}, Value: "left"},
			},
		},
	)
}

func tr(r ...xml.TokenReader) xml.TokenReader {
	return xmlstream.Wrap(
		xmlstream.MultiReader(r...),
		xml.StartElement{Name: xml.Name{Local: "tr"}},
	)
}
