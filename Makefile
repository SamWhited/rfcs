.POSIX:
.SUFFIXES:
.SUFFIXES: .html .pdf .prepped.xml .txt .xml

FILES!=ls *[^d].xml
BUILDABLES=$(FILES:xml=html) $(FILES:xml=pdf) $(FILES:xml=prepped.xml) $(FILES:xml=txt)

.PHONY: all
all: $(BUILDABLES) index.html

.xml.txt:
	xml2rfc --out $@ --text $<

.xml.html:
	xml2rfc --out $@ --html $<

.xml.pdf:
	xml2rfc --out $@ --pdf $<

.xml.prepped.xml:
	xml2rfc --out $@ --prep $<

index.html: $(FILES) $(BUILDABLES)
	tree -T RFCs -H https://rfcs.samwhited.com > index.html
